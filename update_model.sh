#!/bin/bash

# Run the risk score document
R CMD BATCH /home/jim/dev/riskmodel/master_risk_score_1.5.R

# archive the risk score R files
zip archive /home/jim/dev/riskmodel/*.R /home/jim/dev/riskmodel/*.Rnw /home/jim/dev/riskmodel/*.txt /home/jim/dev/riskmodel/*.sh /home/jim/dev/riskmodel/*.sql
echo "backup" | mail -A  /home/jim/dev/riskmodel/archive.zip -s "Backup Files for Espresso" -t "jlaird@espressocapital.com" 
